#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=GNU-Linux-x86
CND_ARTIFACT_DIR_Debug=dist/Debug/GNU-Linux-x86
CND_ARTIFACT_NAME_Debug=guiproject
CND_ARTIFACT_PATH_Debug=dist/Debug/GNU-Linux-x86/guiproject
CND_PACKAGE_DIR_Debug=dist/Debug/GNU-Linux-x86/package
CND_PACKAGE_NAME_Debug=guiproject.tar
CND_PACKAGE_PATH_Debug=dist/Debug/GNU-Linux-x86/package/guiproject.tar
# Release configuration
CND_PLATFORM_Release=GNU-Linux-x86
CND_ARTIFACT_DIR_Release=dist/Release/GNU-Linux-x86
CND_ARTIFACT_NAME_Release=guiproject
CND_ARTIFACT_PATH_Release=dist/Release/GNU-Linux-x86/guiproject
CND_PACKAGE_DIR_Release=dist/Release/GNU-Linux-x86/package
CND_PACKAGE_NAME_Release=guiproject.tar
CND_PACKAGE_PATH_Release=dist/Release/GNU-Linux-x86/package/guiproject.tar
# libRelease configuration
CND_PLATFORM_libRelease=GNU-Linux-x86
CND_ARTIFACT_DIR_libRelease=dist/libRelease/GNU-Linux-x86
CND_ARTIFACT_NAME_libRelease=libguiproject.a
CND_ARTIFACT_PATH_libRelease=dist/libRelease/GNU-Linux-x86/libguiproject.a
CND_PACKAGE_DIR_libRelease=dist/libRelease/GNU-Linux-x86/package
CND_PACKAGE_NAME_libRelease=GuiProject.tar
CND_PACKAGE_PATH_libRelease=dist/libRelease/GNU-Linux-x86/package/GuiProject.tar
# libDebug configuration
CND_PLATFORM_libDebug=GNU-Linux-x86
CND_ARTIFACT_DIR_libDebug=dist/libDebug/GNU-Linux-x86
CND_ARTIFACT_NAME_libDebug=libguiproject.a
CND_ARTIFACT_PATH_libDebug=dist/libDebug/GNU-Linux-x86/libguiproject.a
CND_PACKAGE_DIR_libDebug=dist/libDebug/GNU-Linux-x86/package
CND_PACKAGE_NAME_libDebug=GuiProject.tar
CND_PACKAGE_PATH_libDebug=dist/libDebug/GNU-Linux-x86/package/GuiProject.tar
# win32libRelease configuration
CND_PLATFORM_win32libRelease=MinGW-Windows
CND_ARTIFACT_DIR_win32libRelease=dist/win32libRelease/MinGW-Windows
CND_ARTIFACT_NAME_win32libRelease=libguiproject.a
CND_ARTIFACT_PATH_win32libRelease=dist/win32libRelease/MinGW-Windows/libguiproject.a
CND_PACKAGE_DIR_win32libRelease=dist/win32libRelease/MinGW-Windows/package
CND_PACKAGE_NAME_win32libRelease=GuiProject.tar
CND_PACKAGE_PATH_win32libRelease=dist/win32libRelease/MinGW-Windows/package/GuiProject.tar
# win32libDebug configuration
CND_PLATFORM_win32libDebug=MinGW-Windows
CND_ARTIFACT_DIR_win32libDebug=dist/win32libDebug/MinGW-Windows
CND_ARTIFACT_NAME_win32libDebug=libguiproject.a
CND_ARTIFACT_PATH_win32libDebug=dist/win32libDebug/MinGW-Windows/libguiproject.a
CND_PACKAGE_DIR_win32libDebug=dist/win32libDebug/MinGW-Windows/package
CND_PACKAGE_NAME_win32libDebug=GuiProject.tar
CND_PACKAGE_PATH_win32libDebug=dist/win32libDebug/MinGW-Windows/package/GuiProject.tar
# win32Debug configuration
CND_PLATFORM_win32Debug=MinGW-Windows
CND_ARTIFACT_DIR_win32Debug=dist/win32Debug/MinGW-Windows
CND_ARTIFACT_NAME_win32Debug=guiproject
CND_ARTIFACT_PATH_win32Debug=dist/win32Debug/MinGW-Windows/guiproject
CND_PACKAGE_DIR_win32Debug=dist/win32Debug/MinGW-Windows/package
CND_PACKAGE_NAME_win32Debug=guiproject.tar
CND_PACKAGE_PATH_win32Debug=dist/win32Debug/MinGW-Windows/package/guiproject.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
