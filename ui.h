#ifndef UI_H
#define UI_H

#include <map>
#include <list>
#include <set>
#include <queue>
#include <string>
#include <functional>
#include <glm/glm.hpp>
#include "iinput.h"
#include "font-icons.h"

#include <nanovg.h>

char* cpToUTF8(int cp, char* str);

class Colors
{
public:
    static glm::vec4 Primary;
    static glm::vec4 Secondary;
    static glm::vec4 Bright;
    static glm::vec4 Dark;
    static glm::vec4 Highlight;

    static NVGcolor ToNvgColor(const glm::vec4& color);
};

#define OFFSET_ONE_BUTTON (height * 0.07f)
#define OFFSET_TWO_BUTTONS (OFFSET_ONE_BUTTON * 3.0f)
#define OFFSET_THREE_BUTTONS (OFFSET_ONE_BUTTON * 5.0f)

class Control
{
    std::string _id;
    bool _visible;
protected:
    std::string _text;
    std::string _fontFamily;
    glm::vec2 _position;
    glm::vec2 _size;
    glm::vec2 _padding;
    glm::vec4 _color;
    glm::vec4 _borderColor;
    glm::vec4 _fontColor;
    float _scale;

public:
    Control(const std::string& id);
    virtual ~Control();

    virtual glm::vec2 getEffectiveSize() const;
    virtual glm::vec2 getEffectivePosition() const;

    virtual void update(float elapsed);
    virtual void render(NVGcontext* vg, float scale = 1.0f) = 0;

    const std::string& id() const;

    // Only needed for temporary controls
    virtual bool canBeDeleted() const { return false; }

    void setVisible(bool visible);
    bool visible() const;

    void setText(const std::string& text);
    const std::string& text() const;

    void setFontFamily(const std::string& font);
    const std::string& fontFamily() const;

    void setPosition(const glm::vec2& position);
    const glm::vec2& position() const;

    void setSize(const glm::vec2& size);
    const glm::vec2& size() const;

    void setPadding(const glm::vec2& padding);
    const glm::vec2& padding() const;

    void setColor(const glm::vec4& color);
    const glm::vec4& color() const;

    void setBorderColor(const glm::vec4& color);
    const glm::vec4& borderColor() const;

    void setFontColor(const glm::vec4& color);
    const glm::vec4& fontColor() const;
};

enum class TextAlignments
{
    Center,
    Top,
    TopRight,
    Right,
    BottomRight,
    Bottom,
    BottomLeft,
    Left,
    TopLeft,
};

class Image : public Control
{
    std::string _imageFilename;
    int _image;
public:
    Image(const std::string& id);
    virtual ~Image();

    virtual void render(NVGcontext* vg, float scale = 1.0f);

    void setImage(const std::string& image);
};

class Label : public Control
{
    TextAlignments _textAlignment;
public:
    Label(const std::string& id);
    virtual ~Label();

    virtual void render(NVGcontext* vg, float scale = 1.0f);

    void setTextAlignment(TextAlignments textAlignment);
    TextAlignments textAlignment() const;

};

class FadeOutLabel : public Label
{
    bool _fading;
public:
    FadeOutLabel(const std::string& id);
    virtual ~FadeOutLabel();

    virtual void render(NVGcontext* vg, float scale = 1.0f);
    virtual void update(float elapsed);
    virtual bool canBeDeleted() const;

    void reset();
    bool isFading() const;
    void setFading(bool fading);
    bool isFaded() const;
};

class Button : public Control
{
    eFontAwesomeIcons _icon;
    std::string _iconFontFamily;

public:
    Button(const std::string& id);
    virtual ~Button();

    virtual glm::vec2 getEffectivePosition() const;

    virtual void render(NVGcontext* vg, float scale = 1.0f);

    void setIconFontFamily(const std::string& font);
    const std::string& iconFontFamily() const;

    void setIcon(eFontAwesomeIcons icon);
    eFontAwesomeIcons icon() const;
};

class Star : public Control
{
    eFontAwesomeIcons _icon;
    std::string _iconFontFamily;

public:
    Star(const std::string& id);
    virtual ~Star();

    virtual glm::vec2 getEffectivePosition() const;

    virtual void render(NVGcontext* vg, float scale = 1.0f);

};

class Panel : public Control
{
public:
    Panel(const std::string& id);
    virtual ~Panel();

    virtual void render(NVGcontext* vg, float scale = 1.0f);

};

class ScoreBug : public Control
{
    std::string _score;
    std::string _downAndYards;
    std::string _time;
    std::string _scoreFontFamily;

public:
    ScoreBug(const std::string& id);
    virtual ~ScoreBug();

    virtual void render(NVGcontext* vg, float scale = 1.0f);

    void setScore(int score);
    void setDownAndYards(int down, int yards);
    void setTime(int seconds);

    void setScoreFontFamily(const std::string& font);
    const std::string& scoreFontFamily() const;

    static std::string timeInSecondsToString(int timeInSeconds);
};

class HighScoreList : public Control
{
    class ListItem
    {
    public:
        int _index;
        std::string _name;
        std::string _completion;
        std::string _score;
        std::string _time;
        bool _highlight;
    };
    std::vector<ListItem> _items;

public:
    HighScoreList(const std::string& id);
    virtual ~HighScoreList();

    void reset();
    void addItem(const std::string& name, const std::string& completion, const std::string& score, const std::string& time, bool highlight);
    void replaceItemAt(int at, const std::string& name, const std::string& completion, const std::string& score, const std::string& time, bool highlight, int index);
    virtual void render(NVGcontext* vg, float scale = 1.0f);

};

class UI
{
    const IInput* _input;
    AnalogActionHandle _swipeHandle;
    DigitalActionHandle _startSwipingHandle;

    std::set<FadeOutLabel*> _tempControls;
    std::set<Control*> _controls;
    Control* _hoverControl;
    Control* _clickControl;
    std::queue<Control*> _clickedControls;

    bool _isChangingMode;
    float _changingModeState;

    NVGcontext* vg;

    static UI* _instance;
    UI();
public:
    static UI& Manager();
    virtual ~UI();

    void init(const IInput* input);
    virtual void update(float elapsed);
    virtual void render(int width, int height, float scale = 1.0f);

    FadeOutLabel* addTempControl();

    void addControl(Control* control);
    void removeControl(Control* control);

    void setHoverControl(Control* control);
    const Control* hoverControl() const;
    std::queue<Control*>& clickedControls();

    glm::vec2 currentMousePos();
};

#endif // UI_H
