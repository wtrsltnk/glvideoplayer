#include "Video.h"
#include "glad.h"
#include "input.h"
#include <SDL2/SDL_audio.h>
#include <iostream>
#include "sdl2-setup.h"
#include "ui.h"

static const int BASELINE_WIDTH = 1776 / 2;
static const int BASELINE_HEIGHT = 1080 / 2;

#define FREQ 200 /* the frequency we want */

unsigned int audio_pos; /* which sample we are up to */
int audio_len; /* how many samples left to play, stops when <= 0 */
float audio_frequency; /* audio frequency in cycles per sample */
float audio_volume; /* audio volume, 0 - ~32000 */

class Program : public SDLProgram
{
public:
    Program(
        int width,
        int height)
        : SDLProgram(width, height),
          _showUi(false),
          play(nullptr)
    {}

    virtual bool SetUp();
    virtual void Render();
    virtual void CleanUp();
    virtual void OnResize(int width, int height);

    Video video;
    bool _showUi;
    Button *play;
};

void MyAudioCallback(void* userdata, Uint8* stream, int len) {
  len /= 2; /* 16 bit */
  int i;
  Sint16* buf = (Sint16*)stream;
  for(i = 0; i < len; i++) {
    buf[i] = audio_volume * sin(2 * M_PI * audio_pos * audio_frequency);
    audio_pos++;
  }
  audio_len -= len;
  return;
}

bool Program::SetUp()
{
    for (int i = 0; i < SDL_GetNumAudioDrivers(); ++i)
    {
        const char* driver_name = SDL_GetAudioDriver(i);
        std::cout << "driver #" << i << " : " << driver_name << std::endl;
    }
    SDL_AudioInit("winmm");

    SDL_AudioSpec want, have;
    SDL_zero(want);
    want.freq = 44100;
    want.format = AUDIO_S16;
    want.channels = 1;
    want.samples = 4096;
    want.callback = MyAudioCallback;

    SDL_AudioDeviceID dev = SDL_OpenAudioDevice(NULL, 0, &want, &have, SDL_AUDIO_ALLOW_FREQUENCY_CHANGE);

    audio_len = have.freq * 5; /* 5 seconds */
    audio_pos = 0;
    audio_frequency = 1.0 * FREQ / have.freq; /* 1.0 to make it a float */
    audio_volume = 6000; /* ~1/5 max volume */
    SDL_PauseAudioDevice(dev, 0); /* play! */

    this->video.init();
    UI::Manager().init(this->input());

    if (this->args.size() > 0) this->video.load(this->args[0]);

    glEnable(GL_DEPTH_TEST);
    glClearColor((142.0f / 255.0f), (179.0f / 255.0f), (171.0f / 255.0f), 1.0f);

    this->play = new Button("play");
    this->play->setSize(glm::vec2(48.0f));
    this->play->setPosition(glm::vec2(32.0f, this->height - 32.0f));
    this->play->setIconFontFamily("fontawesome");
    this->play->setIcon(eFontAwesomeIcons::FA_PLAY);
    UI::Manager().addControl(this->play);

    return true;
}

static auto lastUIUpdateTime = 0.0f;

void Program::Render()
{
    auto diff = this->elapsed() - lastUIUpdateTime;
    lastUIUpdateTime = this->elapsed();
    UI::Manager().update(diff);
    while (!UI::Manager().clickedControls().empty())
    {
        auto ctrl = UI::Manager().clickedControls().front();
        UI::Manager().clickedControls().pop();
        if (ctrl == this->play)
        {
            this->video.setPlaying(!this->video.isPlaying());
        }
    }

    this->play->setIcon(this->video.isPlaying() ? eFontAwesomeIcons::FA_PAUSE : eFontAwesomeIcons::FA_PLAY);

    glViewport(0, 0, this->width, this->height);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    this->video.nextFrame(diff);
    this->video.render(this->width, this->height);

    UI::Manager().render(this->width, this->height);
}

void Program::OnResize(int width, int height)
{
    glViewport(0, 0, width, height);

    if (this->play != nullptr)
    {
        this->play->setPosition(glm::vec2(32.0f, height - 32.0f));
    }
}

void Program::CleanUp()
{}

int main(int argc, char *argv[])
{
    return Program(BASELINE_WIDTH, BASELINE_HEIGHT /*1440, 960*/).Run(argc, argv);
}
