#ifndef VIDEO_H_
#define VIDEO_H_

#include <string>
#include "glad.h"

class AVFormatContext;
class AVCodecContext;
class AVCodec;
class AVFrame;

class Video
{
    static GLuint shader();
    static GLint projectionUniform(GLuint shader);
    static GLint viewUniform(GLuint shader);

    int _width;
    int _height;
    int _framerate;
    bool _isPlaying;
    GLuint _vao;
    GLuint _vbo;
public:
	Video();
	virtual ~Video();

    void init();

    bool load(const std::string& filename);

    void nextFrame(float diff);
    void render(int w, int h);

	void uploadFrame(AVFrame* pFrame, int width, int height);

    bool isPlaying() const;
    void setPlaying(bool playing);
private:
	AVFormatContext* m_pFormatCtx;
	AVCodecContext* m_pCodecCtx;
	AVCodec* m_pCodec;
	AVFrame* m_pFrame;
	AVFrame* m_pFrameRGB;
	unsigned char* m_pBuffer;
	int m_nBufferSize;
	int m_nVideoStream;
	unsigned int m_unCurrentFrameGLIndex;

};

#endif /* VIDEO_H_ */
