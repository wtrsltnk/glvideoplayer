#include "ui.h"
#include "input.h"

#include "platform-opengl.h"

#include "libs/nanovg/src/nanovg.h"

#ifdef __ANDROID__
#define NANOVG_GLES3
#else
#define NANOVG_GL3
#endif

#include "libs/nanovg/src/nanovg_gl.h"

#include "font-icons.h"
#include "log.h"

#include <algorithm>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iomanip>
#include <iostream>
#include <sstream>

glm::vec4 Colors::Primary = glm::vec4(57, 102, 46, 255);
glm::vec4 Colors::Secondary = glm::vec4(31, 79, 141, 255);
glm::vec4 Colors::Bright = glm::vec4(255, 255, 255, 255);
glm::vec4 Colors::Dark = glm::vec4(0, 45, 0, 255);
glm::vec4 Colors::Highlight = glm::vec4(161, 77, 49, 255);

NVGcolor Colors::ToNvgColor(const glm::vec4 &color)
{
    return nvgRGBA(color.r, color.g, color.b, color.a);
}

#define ICON_SEARCH 0x1F50D
#define ICON_CIRCLED_CROSS 0x2716
#define ICON_CHEVRON_RIGHT 0xE75E
#define ICON_CHECK 0x2713
#define ICON_LOGIN 0xE740
#define ICON_TRASH 0xE729
#define ICON_RETRY 0x27F2

char *cpToUTF8(int cp, char *str)
{
    int n = 0;
    if (cp < 0x80)
        n = 1;
    else if (cp < 0x800)
        n = 2;
    else if (cp < 0x10000)
        n = 3;
    else if (cp < 0x200000)
        n = 4;
    else if (cp < 0x4000000)
        n = 5;
    else if (cp <= 0x7fffffff)
        n = 6;
    str[n] = '\0';
    switch (n)
    {
        case 6:
            str[5] = 0x80 | (cp & 0x3f);
            cp = cp >> 6;
            cp |= 0x4000000;
        case 5:
            str[4] = 0x80 | (cp & 0x3f);
            cp = cp >> 6;
            cp |= 0x200000;
        case 4:
            str[3] = 0x80 | (cp & 0x3f);
            cp = cp >> 6;
            cp |= 0x10000;
        case 3:
            str[2] = 0x80 | (cp & 0x3f);
            cp = cp >> 6;
            cp |= 0x800;
        case 2:
            str[1] = 0x80 | (cp & 0x3f);
            cp = cp >> 6;
            cp |= 0xc0;
        case 1:
            str[0] = cp;
    }
    return str;
}

UI *UI::_instance = nullptr;

UI::UI()
    : _input(nullptr),
      _swipeHandle(0),
      _startSwipingHandle(0),
      _hoverControl(nullptr),
      _clickControl(nullptr),
      _isChangingMode(false),
      _changingModeState(0.0f),
      vg(nullptr)
{}

UI &UI::Manager()
{
    if (UI::_instance == nullptr) UI::_instance = new UI();

    return *UI::_instance;
}

UI::~UI()
{}

void UI::init(const IInput *input)
{
    this->_input = input;
    this->_swipeHandle = this->_input->getAnalogActionHandle("throwing");
    this->_startSwipingHandle = this->_input->getDigitalActionHandle("start_throw");

#ifdef __ANDROID__
    this->vg = nvgCreateGLES3(NVG_ANTIALIAS | NVG_STENCIL_STROKES | NVG_DEBUG);
#else
    this->vg = nvgCreateGL3(NVG_ANTIALIAS | NVG_STENCIL_STROKES | NVG_DEBUG);
#endif

    auto fontawesome = nvgCreateFont(this->vg, "fontawesome", "fontawesome-webfont.ttf");
    if (fontawesome == -1)
    {
        printf("Could not add fontawesome.\n");
        return;
    }
}

float menuSlideSpeed = 3.0f;

void UI::update(float elapsed)
{
    if (this->_isChangingMode)
    {
        this->_changingModeState += (elapsed * menuSlideSpeed);
        if (this->_changingModeState > 1.0f)
        {
            this->_isChangingMode = false;
            this->_changingModeState = 1.0f;
        }
        return;
    }

    for (auto control : this->_controls)
        if (control->visible()) control->update(elapsed);

    for (auto control : this->_tempControls)
        control->update(elapsed);

    if (this->_input->getDigitalActionData(this->_startSwipingHandle).state)
    {
        if (this->_clickControl != this->_hoverControl) this->_clickControl = this->_hoverControl;
    }

    if (!this->_input->getDigitalActionData(this->_startSwipingHandle).state)
    {
        if (this->_clickControl != nullptr)
        {
            this->_clickedControls.push(this->_clickControl);
            this->_clickControl = nullptr;
        }
    }
}

void UI::render(int width, int height, float scale)
{
    nvgBeginFrame(this->vg, width, height, float(width) / float(height));

    nvgSave(this->vg);

    for (auto control : this->_controls)
        if (control->visible()) control->render(this->vg, scale);

    nvgRestore(this->vg);

    for (auto control : this->_tempControls)
        if (control->isFading()) control->render(this->vg, scale);

    nvgEndFrame(this->vg);
}

FadeOutLabel *UI::addTempControl()
{
    for (FadeOutLabel *c : this->_tempControls)
    {
        if (c->isFaded())
        {
            c->reset();
            return c;
        }
    }

    auto control = new FadeOutLabel("fade-out-label");
    control->setFontFamily("collegiate");
    control->setSize(glm::vec2(32.0f));
    control->setColor(glm::vec4(255, 255, 255, 255));
    control->setFading(true);
    this->_tempControls.insert(control);
    return control;
}

void UI::addControl(Control *control)
{
    this->_controls.insert(control);
}

void UI::removeControl(Control *control)
{
    if (this->_controls.find(control) != this->_controls.end())
        this->_controls.erase(control);
}

void UI::setHoverControl(Control *control)
{
    this->_hoverControl = control;
}

const Control *UI::hoverControl() const
{
    return this->_hoverControl;
}

std::queue<Control *> &UI::clickedControls()
{
    return this->_clickedControls;
}

glm::vec2 UI::currentMousePos()
{
    auto data = this->_input->getAnalogActionData(this->_swipeHandle);
    return glm::vec2(data.x, data.y);
}

Control::Control(
    const std::string &id)
    : _id(id),
      _visible(true),
      _fontFamily("sans-bold"),
      _color(glm::vec4(255, 255, 255, 255)),
      _scale(1.0f)
{}

Control::~Control()
{}

glm::vec2 Control::getEffectiveSize() const
{
    return this->size();
}

glm::vec2 Control::getEffectivePosition() const
{
    return this->position();
}

void Control::update(float elapsed)
{
    auto s = this->getEffectiveSize();
    auto p = this->getEffectivePosition();
    auto m = UI::Manager().currentMousePos();

    if (m.x >= p.x && m.x <= (p.x + s.x) && m.y <= (p.y + s.y) && m.y >= p.y)
    {
        UI::Manager().setHoverControl(this);
    }
    else if (UI::Manager().hoverControl() == this)
    {
        UI::Manager().setHoverControl(nullptr);
    }

    if (UI::Manager().hoverControl() == this)
    {
        this->_scale += 1.0f * elapsed;
        if (this->_scale > 1.1f) this->_scale = 1.1f;
    }
    else if (this->_scale > 1.0f)
    {
        this->_scale -= 1.0f * elapsed;
        if (this->_scale < 1.0f) this->_scale = 1.0f;
    }
}

const std::string &Control::id() const
{
    return this->_id;
}

void Control::setVisible(bool visible)
{
    this->_visible = visible;
}

bool Control::visible() const
{
    return this->_visible;
}

void Control::setText(const std::string &text)
{
    this->_text = text;
}

const std::string &Control::text() const
{
    return this->_text;
}

void Control::setFontFamily(const std::string &font)
{
    this->_fontFamily = font;
}

const std::string &Control::fontFamily() const
{
    return this->_fontFamily;
}

void Control::setPosition(const glm::vec2 &position)
{
    this->_position = position;
}

const glm::vec2 &Control::position() const
{
    return this->_position;
}

void Control::setSize(const glm::vec2 &size)
{
    this->_size = size;
}

const glm::vec2 &Control::size() const
{
    return this->_size;
}

void Control::setPadding(const glm::vec2 &padding)
{
    this->_padding = padding;
}

const glm::vec2 &Control::padding() const
{
    return this->_padding;
}

void Control::setColor(const glm::vec4 &color)
{
    this->_color = color;
}

const glm::vec4 &Control::color() const
{
    return this->_color;
}

void Control::setBorderColor(const glm::vec4 &color)
{
    this->_borderColor = color;
}

const glm::vec4 &Control::borderColor() const
{
    return this->_borderColor;
}

void Control::setFontColor(const glm::vec4 &color)
{
    this->_fontColor = color;
}

const glm::vec4 &Control::fontColor() const
{
    return this->_fontColor;
}

Image::Image(const std::string &id)
    : Control(id), _image(0)
{}

Image::~Image()
{}

void Image::render(NVGcontext *vg, float scale)
{
    if (this->_image == 0) this->_image = nvgCreateImage(vg, this->_imageFilename.c_str(), 0);

    nvgSave(vg);

    nvgTranslate(vg, this->_position.x, this->_position.y);
    nvgScale(vg, scale, scale);

    auto imgPaint = nvgImagePattern(vg, 0, 0, this->size().x, this->size().y, 0.0f / 180.0f * NVG_PI, this->_image, this->color().a / 255.0f);
    nvgBeginPath(vg);
    nvgRect(vg, 0, 0, this->size().x, this->size().y);
    nvgFillPaint(vg, imgPaint);
    nvgFill(vg);

    nvgRestore(vg);
}

void Image::setImage(const std::string &image)
{
    this->_imageFilename = image;
    this->_image = 0;
}

Label::Label(const std::string &id)
    : Control(id), _textAlignment(TextAlignments::Center)
{
    this->setFontColor(Colors::Bright);
}

Label::~Label()
{}

void Label::render(NVGcontext *vg, float scale)
{
    nvgSave(vg);

    nvgTranslate(vg, this->_position.x, this->_position.y);
    nvgScale(vg, scale, scale);

    nvgFontSize(vg, this->size().y);
    nvgFontFace(vg, this->fontFamily().c_str());
    nvgFillColor(vg, Colors::ToNvgColor(this->fontColor()));
    switch (this->_textAlignment)
    {
        case TextAlignments::Center:
            nvgTextAlign(vg, NVG_ALIGN_CENTER | NVG_ALIGN_MIDDLE);
            break;
        case TextAlignments::Bottom:
            nvgTextAlign(vg, NVG_ALIGN_CENTER | NVG_ALIGN_BOTTOM);
            break;
        case TextAlignments::BottomLeft:
            nvgTextAlign(vg, NVG_ALIGN_LEFT | NVG_ALIGN_BOTTOM);
            break;
        case TextAlignments::BottomRight:
            nvgTextAlign(vg, NVG_ALIGN_RIGHT | NVG_ALIGN_BOTTOM);
            break;
        case TextAlignments::Left:
            nvgTextAlign(vg, NVG_ALIGN_LEFT | NVG_ALIGN_MIDDLE);
            break;
        case TextAlignments::Right:
            nvgTextAlign(vg, NVG_ALIGN_RIGHT | NVG_ALIGN_MIDDLE);
            break;
        case TextAlignments::Top:
            nvgTextAlign(vg, NVG_ALIGN_CENTER | NVG_ALIGN_TOP);
            break;
        case TextAlignments::TopLeft:
            nvgTextAlign(vg, NVG_ALIGN_LEFT | NVG_ALIGN_TOP);
            break;
        case TextAlignments::TopRight:
            nvgTextAlign(vg, NVG_ALIGN_RIGHT | NVG_ALIGN_TOP);
            break;
    }
    nvgText(vg, 0.0f, 0.0f, this->_text.c_str(), NULL);

    nvgRestore(vg);
}

void Label::setTextAlignment(TextAlignments textAlignment)
{
    this->_textAlignment = textAlignment;
}

TextAlignments Label::textAlignment() const
{
    return this->_textAlignment;
}

FadeOutLabel::FadeOutLabel(const std::string &id)
    : Label(id), _fading(false)
{}

FadeOutLabel::~FadeOutLabel()
{}

void FadeOutLabel::render(NVGcontext *vg, float scale)
{
    Label::render(vg, scale + (1.0f - (this->fontColor().a / 255.0f)));
}

void FadeOutLabel::update(float elapsed)
{
    if (this->isFading())
    {
        this->_fontColor.a -= (100.0f * elapsed);
        this->setFading(this->fontColor().a > 0.0f);
    }
}

bool FadeOutLabel::canBeDeleted() const
{
    return this->isFaded();
}

void FadeOutLabel::reset()
{
    this->setFontColor(glm::vec4(fontColor().r, fontColor().g, fontColor().b, 255));
    this->setFading(true);
}

bool FadeOutLabel::isFading() const
{
    return this->_fading;
}

void FadeOutLabel::setFading(bool fading)
{
    this->_fading = fading;
}

bool FadeOutLabel::isFaded() const
{
    return this->fontColor().a < 0.0f;
}

Button::Button(const std::string &id)
    : Control(id), _icon(eFontAwesomeIcons::FA_UNDO), _iconFontFamily("fontawesome")
{
    this->setColor(Colors::Secondary);
    this->setFontColor(Colors::Bright);
    this->setBorderColor(Colors::Dark);
    this->setSize(glm::vec2(60.0f, 60.0f));
}

Button::~Button()
{}

glm::vec2 Button::getEffectivePosition() const
{
    return this->position() - (this->size() / 2.0f);
}

void Button::setIconFontFamily(const std::string &font)
{
    this->_iconFontFamily = font;
}

const std::string &Button::iconFontFamily() const
{
    return this->_iconFontFamily;
}

void Button::setIcon(eFontAwesomeIcons icon)
{
    this->_icon = icon;
}

eFontAwesomeIcons Button::icon() const
{
    return this->_icon;
}

void Button::render(NVGcontext *vg, float scale)
{
    nvgSave(vg);

    nvgTranslate(vg, this->_position.x, this->_position.y);

    nvgScale(vg, scale * this->_scale, scale * this->_scale);

    nvgBeginPath(vg);
    nvgRoundedRect(vg, -(this->size().x / 2.0f), -(this->size().y / 2.0f), this->size().x, this->size().y, this->size().y / 2.0f);
    nvgFillColor(vg, Colors::ToNvgColor(this->color()));
    nvgFill(vg);

    char icon[8];
    nvgFontSize(vg, (this->size().x * 0.3f));
    nvgFontFace(vg, this->iconFontFamily().c_str());
    nvgFillColor(vg, Colors::ToNvgColor(this->fontColor()));
    nvgTextAlign(vg, NVG_ALIGN_CENTER | NVG_ALIGN_MIDDLE);
    nvgText(vg, 0.0f, 0.0f, cpToUTF8(int(this->_icon), icon), NULL);

    nvgRestore(vg);
}

Star::Star(const std::string &id)
    : Control(id), _icon(eFontAwesomeIcons::FA_STAR), _iconFontFamily("fontawesome")
{
    this->setSize(glm::vec2(120.0f));
}

Star::~Star()
{}

glm::vec2 Star::getEffectivePosition() const
{
    return this->position() - (this->size() / 2.0f);
}

void Star::render(NVGcontext *vg, float scale)
{
    nvgSave(vg);

    nvgTranslate(vg, this->_position.x, this->_position.y);

    nvgScale(vg, scale, scale);

    char icon[8];
    nvgFontSize(vg, (this->size().x * 0.5f));
    nvgFontFace(vg, this->_iconFontFamily.c_str());
    nvgFillColor(vg, Colors::ToNvgColor(this->color()));
    nvgTextAlign(vg, NVG_ALIGN_CENTER | NVG_ALIGN_MIDDLE);
    nvgText(vg, 0.0f, 0.0f, cpToUTF8(int(this->_icon), icon), NULL);

    nvgRestore(vg);
}

Panel::Panel(const std::string &id)
    : Control(id)
{
    this->setColor(Colors::Primary);
    this->setBorderColor(Colors::Dark);
}

Panel::~Panel()
{}

void Panel::render(NVGcontext *vg, float scale)
{
    nvgSave(vg);

    nvgTranslate(vg, this->position().x, this->position().y);
    nvgScale(vg, scale, scale);

    auto borderRadius = 0.0f;

    nvgBeginPath(vg);
    nvgRect(vg, -(this->size().x / 2.0f) + 4.0f, -(this->size().y / 2.0f) + 4.0f, this->size().x - 2.0f, this->size().y - 2.0f);
    nvgFillColor(vg, Colors::ToNvgColor(this->borderColor()));
    nvgFill(vg);

    nvgBeginPath(vg);
    nvgRoundedRect(vg, -(this->size().x / 2.0f), -(this->size().y / 2.0f), this->size().x, this->size().y, borderRadius);
    nvgFillColor(vg, Colors::ToNvgColor(this->color()));
    nvgFill(vg);

    nvgRestore(vg);
}

ScoreBug::ScoreBug(const std::string &id)
    : Control(id), _scoreFontFamily("collegiate")
{
    this->setSize(glm::vec2(320, 50));
}

ScoreBug::~ScoreBug()
{}

void ScoreBug::setScore(int score)
{
    std::stringstream ss;
    ss << score;
    this->_score = ss.str();
}

void ScoreBug::setDownAndYards(int down, int yards)
{
    std::stringstream ss;
    ss << down << (down == 1 ? "st" : (down == 2 ? "nd" : (down == 3 ? "rd" : "th"))) << " & " << (yards > 0 ? yards : 0);
    this->_downAndYards = ss.str();
}

void ScoreBug::setTime(int seconds)
{
    this->_time = ScoreBug::timeInSecondsToString(seconds);
}

std::string ScoreBug::timeInSecondsToString(int timeInSeconds)
{
    std::stringstream ss;
    ss << (timeInSeconds / 60) << ":" << std::setw(2) << std::setfill('0') << (timeInSeconds % 60);
    return ss.str();
}

void ScoreBug::render(NVGcontext *vg, float scale)
{
    nvgSave(vg);

    nvgTranslate(vg, this->position().x, this->position().y);
    nvgScale(vg, scale, scale);

    auto scoreScale = 7.0f;
    nvgBeginPath(vg);
    nvgRect(vg, -(this->size().x / 2.0f), (this->size().y * 0.6f), this->size().x, (this->size().y * 0.6f));
    nvgFillColor(vg, Colors::ToNvgColor(Colors::Secondary));
    nvgFill(vg);

    nvgBeginPath(vg);
    nvgRect(vg, -(this->size().x / 2.0f), (this->size().y * 0.6f), this->size().x, (this->size().y * 0.6f));
    nvgStrokeColor(vg, Colors::ToNvgColor(Colors::Secondary));
    nvgStrokeWidth(vg, 4.0f);
    nvgStroke(vg);

    // grijze vak
    nvgBeginPath(vg);
    nvgRect(vg, -(this->size().x / 2.0f), (this->size().y * 0.2f), this->size().x, (this->size().y * 0.6f));
    nvgStrokeColor(vg, Colors::ToNvgColor(Colors::Bright));
    nvgStrokeWidth(vg, 4.0f);
    nvgStroke(vg);

    nvgBeginPath(vg);
    nvgRect(vg, -(this->size().x / 2.0f), (this->size().y * 0.2f), this->size().x, (this->size().y * 0.6f));
    nvgFillColor(vg, nvgRGBA(114, 104, 95, 255));
    nvgFill(vg);

    // bottom
    nvgBeginPath(vg);
    nvgRect(vg, -(this->size().x / scoreScale), -(this->size().y * 0.05f), (this->size().x / (scoreScale / 2.0f)), (this->size().y * 1.1f));
    nvgFillColor(vg, nvgRGBA(243, 226, 172, 255));
    nvgFill(vg);

    nvgBeginPath(vg);
    nvgRect(vg, -(this->size().x / scoreScale), -(this->size().y * 0.05f), (this->size().x / (scoreScale / 2.0f)), (this->size().y * 1.1f));
    nvgFillColor(vg, Colors::ToNvgColor(Colors::Secondary));
    nvgFill(vg);

    // score box
    nvgBeginPath(vg);
    nvgRect(vg, -(this->size().x / (scoreScale * 1.1f)), (this->size().y * 0.05f), (this->size().x / (scoreScale / 1.8f)), this->size().y);
    nvgFillColor(vg, nvgRGBA(255, 255, 255, 100));
    nvgFill(vg);

    // texts
    nvgFontSize(vg, this->size().y);
    nvgFontFace(vg, this->scoreFontFamily().c_str());
    nvgFillColor(vg, Colors::ToNvgColor(Colors::Bright));
    nvgTextAlign(vg, NVG_ALIGN_CENTER | NVG_ALIGN_MIDDLE);
    nvgText(vg, 0.0f, (this->size().y / 2.0f), this->_score.c_str(), NULL);

    nvgFontSize(vg, this->size().y * 0.55f);
    nvgFontFace(vg, this->fontFamily().c_str());
    nvgFillColor(vg, Colors::ToNvgColor(Colors::Bright));
    nvgTextAlign(vg, NVG_ALIGN_CENTER | NVG_ALIGN_MIDDLE);
    nvgText(vg, -(this->size().x / 3.0f), (this->size().y / 2.0f), this->_downAndYards.c_str(), NULL);

    char icon[8];
    nvgFontSize(vg, 20);
    nvgFontFace(vg, "fontawesome");
    nvgFillColor(vg, Colors::ToNvgColor(Colors::Bright));
    nvgTextAlign(vg, NVG_ALIGN_CENTER | NVG_ALIGN_MIDDLE);
    nvgText(vg, (this->size().x / 4.0f), (this->size().y / 2.0f), cpToUTF8(int(eFontAwesomeIcons::FA_CLOCK_O), icon), NULL);

    nvgFontSize(vg, this->size().y * 0.55f);
    nvgFontFace(vg, this->fontFamily().c_str());
    nvgFillColor(vg, Colors::ToNvgColor(Colors::Bright));
    nvgTextAlign(vg, NVG_ALIGN_LEFT | NVG_ALIGN_MIDDLE);
    nvgText(vg, (this->size().x / 3.3f), (this->size().y / 2.0f), this->_time.c_str(), NULL);

    nvgRestore(vg);
}

void ScoreBug::setScoreFontFamily(const std::string &font)
{
    this->_scoreFontFamily = font;
}

const std::string &ScoreBug::scoreFontFamily() const
{
    return this->_scoreFontFamily;
}

HighScoreList::HighScoreList(const std::string &id)
    : Control(id)
{}

HighScoreList::~HighScoreList()
{}

void HighScoreList::reset()
{
    this->_items.clear();
}

void HighScoreList::addItem(const std::string &name, const std::string &completion, const std::string &score, const std::string &time, bool highlight)
{
    ListItem item;
    item._index = this->_items.size() + 1;
    item._highlight = highlight;
    item._name = name;
    item._completion = completion;
    item._score = score;
    item._time = time;
    this->_items.push_back(item);
}

void HighScoreList::replaceItemAt(int at, const std::string &name, const std::string &completion, const std::string &score, const std::string &time, bool highlight, int index)
{
    ListItem &item = this->_items.at(at);

    item._index = index;
    item._highlight = highlight;
    item._name = name;
    item._completion = completion;
    item._score = score;
    item._time = time;
}

void HighScoreList::render(NVGcontext *vg, float scale)
{
    auto size = glm::vec2(550.0f, 350.0f);
    auto rowHeight = size.y / 6.0f;

    std::stringstream ss;
    ss << "size(" << size.x << ", " << size.y << ")\nscale(" << scale << ")";
    Log::Current().Info(ss.str().c_str());
    nvgSave(vg);

    nvgTranslate(vg, this->position().x, this->position().y);
    nvgScale(vg, scale, scale);

    auto borderRadius = 5.0f;

    nvgBeginPath(vg);
    nvgRoundedRect(vg, -(size.x / 2.0f), -(size.y / 2.0f), size.x, size.y, borderRadius);
    nvgFillColor(vg, Colors::ToNvgColor(this->color()));
    nvgFill(vg);

    nvgBeginPath(vg);
    nvgRoundedRect(vg, -(size.x / 2.0f), -(size.y / 2.0f), size.x, size.y, borderRadius);
    nvgStrokeColor(vg, Colors::ToNvgColor(this->borderColor()));
    nvgStrokeWidth(vg, 4.0f);
    nvgStroke(vg);

    float y = -(size.y / 2.0f) + (rowHeight / 2.0f); // add the half row heigth to make sure the texts are in the middle of the row

    nvgFontSize(vg, 32.0f);
    nvgFontFace(vg, this->fontFamily().c_str());
    nvgFillColor(vg, Colors::ToNvgColor(this->fontColor()));
    nvgTextAlign(vg, NVG_ALIGN_CENTER | NVG_ALIGN_MIDDLE);
    nvgText(vg, 0.0f, y, this->text().c_str(), NULL);

    for (unsigned int i = 0; i < 5 && i < this->_items.size(); i++)
    {
        y += rowHeight;

        char str[32] = {0};
        sprintf(str, "%d", this->_items[i]._index);
        nvgFontFace(vg, this->fontFamily().c_str());

        if (this->_items[i]._highlight)
        {
            nvgFontSize(vg, 36.0f);
            nvgFillColor(vg, nvgRGBA(229, 203, 44, 255));
        }
        else
        {
            nvgFontSize(vg, 32.0f);
            nvgFillColor(vg, Colors::ToNvgColor(this->fontColor()));
        }
        nvgTextAlign(vg, NVG_ALIGN_CENTER | NVG_ALIGN_MIDDLE);
        nvgText(vg, -240.0f, y, str, NULL);

        nvgTextAlign(vg, NVG_ALIGN_LEFT | NVG_ALIGN_MIDDLE);
        nvgText(vg, -190.0f, y, this->_items[i]._name.c_str(), NULL);

        nvgFontSize(vg, 32.0f);
        nvgTextAlign(vg, NVG_ALIGN_RIGHT | NVG_ALIGN_MIDDLE);
        nvgText(vg, 50.0f, y, this->_items[i]._completion.c_str(), NULL);

        nvgFontSize(vg, 32.0f);
        nvgTextAlign(vg, NVG_ALIGN_RIGHT | NVG_ALIGN_MIDDLE);
        nvgText(vg, 130.0f, y, this->_items[i]._score.c_str(), NULL);

        nvgTextAlign(vg, NVG_ALIGN_LEFT | NVG_ALIGN_MIDDLE);
        nvgText(vg, 200.0f, y, this->_items[i]._time.c_str(), NULL);

        char icon[8];
        nvgFontSize(vg, 20);
        nvgFontFace(vg, "fontawesome");
        nvgTextAlign(vg, NVG_ALIGN_CENTER | NVG_ALIGN_MIDDLE);
        nvgText(vg, 180.0f, y, cpToUTF8(int(eFontAwesomeIcons::FA_CLOCK_O), icon), NULL);
    }
    nvgRestore(vg);
}
